# fungsi fahrenheit
def Fahrenheit():
    # Fahrenheit
    F = float(input("Masukan suhu Fahrenheit : "))
    print('suhu adalah', F, 'Fahrenheit')
    # -celcius-
    C = (5 / 9) * (F - 32)
    print('suhu adalah', C, 'Celcius')
    # -reamur-
    R = (4 / 9) * (F - 32)
    print('suhu adalah', R, 'Reamur')
    # -kelvin-
    K = (5 / 9) * (F - 32) + 273
    print('suhu adalah', K, 'Kelvin')

    while True:
        tanya = input("\nMasih lagi? y/n? ")
        if tanya == "y":
            Pertanyaan()
        elif tanya == "n":
            quit()
        else:
            print("\npilih y / n tok\n")

# fungsi kelvin
def Kelvin():
    # Kelvin
    K = float(input('Masukan suhu Kelvin : '))
    print('suhu adalah', K, 'Kelvin')
    # -celcius-
    C = K - 273
    print('suhu adalah', C, 'Celcius')
    # -reamur-
    R = (4 / 5) * (K - 273)
    print('suhu adalah', R, 'Reamur')
    # -fahrenheit-
    F = (9 / 5) * (K - 273) + 32
    print('suhu adalah', F, 'Fahrenheit')

    while True:
        tanya = input("Masih lagi? y/n? ")
        if tanya == "y":
            Pertanyaan()
        elif tanya == "n":
            quit()
        else:
            print("\npilih y / n tok\n")

def Pertanyaan():
    while True:
        print(
        """
        KALKULATOR SUHU
        pilih :
            1. Suhu Fahrenheit
            2. Suhu Kelvin
            0. Exit
        """
        )
        pilihan = input("Pilih nomer suhu = ")
        
        # ketika angka tanpa tanda "" maka bertype integer, tapi jika angka di beri tanda "" maka bertype string
        if pilihan == "1":
            Fahrenheit()
        elif pilihan == '2':
            Kelvin()
        elif pilihan == "0":
            quit()
        else:
            print("\npilih 1 atau 2 tok\n")

# di panggil di akhir
Pertanyaan()